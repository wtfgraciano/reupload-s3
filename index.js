const { S3 } = require('aws-sdk');
const {
  bucket,
  path,
  keyPattern,
  objects,
} = require('./config.json');

const s3 = new S3();

const isOnPattern = ({ Key }) => !keyPattern || Key.match(keyPattern);
const isOnObjectsArray = ({ Key }) => objects.length === 0
  || objects.find(obj => Key.match(obj));

const reupload = async ({ Key, Bucket }) => {
  const { Body } = await s3.getObject({ Key, Bucket }).promise();
  return s3.upload({ Bucket, Key, Body }).promise();
}

(async () => {
  const { Contents } = await s3.listObjectsV2({
    Bucket: bucket,
    Prefix: path,
  }).promise();
  const desiredObjects = Contents
    .filter(isOnPattern)
    .filter(isOnObjectsArray)
    .map(params => ({
      ...params,
      Bucket: bucket,
    }));
  return Promise.all(desiredObjects.map(reupload));
})().then(console.log).catch(console.log);