Reupload S3
===========

## What

At my job, we have multiple integrations that when some new file is uploaded to specific buckets the files are also sent to legacy structures from our partners. Sometimes those legacy structures stop working, so people indentify what files they need and we reupload them, triggering the integration again.

## How

Clone the project, `npm install` the dependencies and copy the `config.example.json` to a `config.json` file. There you can make your input about what will be uploaded:

 - `bucket` is required
 - `path` is not required
 - `keyPattern` is not required
 - `objects` must be an array. If it is empty, **ALL YOUR FILES WILL BE REUPLOADED** 

Then, `npm start` will do the job.

## to-do

I don't know, maybe load the config from cli args so it can be used with npx?